package com.example.ssbookstoreapplication.view

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.ssbookstoreapplication.R
import com.example.ssbookstoreapplication.databinding.ActivityBookDetailsBinding
import com.example.ssbookstoreapplication.model.*
import java.text.SimpleDateFormat
import java.util.*

class BookDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBookDetailsBinding
    var userChosenOption: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
        val book = bookFromID(bookID)

        binding.saveButton.setOnClickListener {
            if (!isEdit && binding.saveButton.text.equals(getString(R.string.edit))) {
                binding.saveButton.text = getString(R.string.done)
                isEdit = true
                val intent = Intent(applicationContext, BookDetailsActivity::class.java)
                intent.putExtra(BOOK_EXTRA, book?.id)
                startActivity(intent)
            } else if (isEdit) {
                binding.imageView2.invalidate()
                val imageDrawable = binding.imageView2.drawable
                val imageBitmap = imageDrawable.toBitmap()
                val isTextFieldValid: Boolean = validateTextField()
                if (isTextFieldValid) {
                    book?.bookImage = imageBitmap
                    book?.bookName = binding.editTextTextBookName.text.toString()
                    book?.authorName = binding.editTextTextAuthorName.text.toString()
                    book?.description = binding.editTextTextBookDescription.text.toString()
                    isEdit = false
                    finish()
                } else {
                    Toast.makeText(
                        this,
                        "Book name & author name cannot be empty.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            } else {
                binding.imageView2.invalidate()
                val imageDrawable = binding.imageView2.drawable
                val imageBitmap = imageDrawable.toBitmap()
                val isTextFieldValid: Boolean = validateTextField()
                if (isTextFieldValid) {
                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val currentDate = sdf.format(Date())
                    val book = Book(
                        bookImage = imageBitmap,
                        bookName = binding.editTextTextBookName.text.toString(),
                        authorName = binding.editTextTextAuthorName.text.toString(),
                        dateCreated = currentDate,
                        description = binding.editTextTextBookDescription.text.toString()
                    )
                    booklist.add(book)
                    finish()
                } else {
                    Toast.makeText(
                        this,
                        "Book name & author name cannot be empty.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        binding.editTextTextBookName.setOnClickListener {
            val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
            val book = bookFromID(bookID)
            if (book != null && !isEdit) {
                Toast.makeText(
                    this,
                    "Select 'Edit' button to start editing",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else {
                return@setOnClickListener
            }
        }

        binding.editTextTextAuthorName.setOnClickListener {
            val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
            val book = bookFromID(bookID)
            if (book != null && !isEdit) {
                Toast.makeText(
                    this,
                    "Select 'Edit' button to start editing",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else {
                return@setOnClickListener
            }
        }

        binding.editTextTextBookDescription.setOnClickListener {
            val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
            val book = bookFromID(bookID)
            if (book != null && !isEdit) {
                Toast.makeText(
                    this,
                    "Select 'Edit' button to start editing",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else {
                return@setOnClickListener
            }
        }

        binding.imageView2.setOnClickListener {
            val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
            val book = bookFromID(bookID)
            if (book != null && !isEdit) {
                Toast.makeText(
                    this,
                    "Select 'Edit' button to start editing",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else {
                selectImageView()
            }
        }

        binding.backButton.setOnClickListener {
            if (isEdit) {
                binding.saveButton.text = getString(R.string.edit)
                isEdit = false
            }
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        val bookID = intent.getIntExtra(BOOK_EXTRA, -1)
        val book = bookFromID(bookID)
        if (book != null && !isEdit) {
            binding.saveButton.text = getString(R.string.edit)
            binding.selectImageTextView.visibility = View.INVISIBLE
            binding.imageView2.setImageBitmap(book.bookImage)
            binding.imageView2.isFocusable = false
            binding.editTextTextBookName.setText(book.bookName)
            binding.editTextTextBookName.isFocusable = false
            binding.editTextTextBookName.alpha = 0.5f
            binding.editTextTextAuthorName.setText(book.authorName)
            binding.editTextTextAuthorName.isFocusable = false
            binding.editTextTextAuthorName.alpha = 0.5f
            binding.editTextTextBookDescription.setText(book.description)
            binding.editTextTextBookDescription.isFocusable = false
            binding.editTextTextBookDescription.alpha = 0.5f
        } else if (book != null && isEdit) {
            binding.selectImageTextView.visibility = View.VISIBLE
            binding.imageView2.setImageBitmap(book.bookImage)
            binding.imageView2.isFocusable = true
            binding.editTextTextBookName.setText(book.bookName)
            binding.editTextTextBookName.isFocusable = true
            binding.editTextTextAuthorName.setText(book.authorName)
            binding.editTextTextAuthorName.isFocusable = true
            binding.editTextTextBookDescription.setText(book.description)
            binding.editTextTextBookDescription.isFocusable = true
        }
    }

    private fun validateTextField(): Boolean {
        val isBookNameNull: Boolean = (binding.editTextTextBookName.text.toString().isEmpty())
        val isAuthorNameNull: Boolean = (binding.editTextTextAuthorName.text.toString().isEmpty())
        return !isBookNameNull && !isAuthorNameNull
    }

    private fun bookFromID(bookID: Int): Book? {

        for (book in booklist) {
            if (book.id == bookID)
                return book
        }
        return null
    }

    private fun selectImageView() {
        val listOption = arrayOf("Photo Library", "Camera", "Cancel")
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle("Select Image from:")
        dialogBuilder.setCancelable(false)
        dialogBuilder.setItems(listOption) { dialog, item ->
            if (item == 0) {
                if (isGalleryPermissionsAllowed()) {
                    userChosenOption = listOption[item]
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
            } else if (item == 1) {
                if (isCameraPermissionsAllowed()) {
                    userChosenOption = listOption[item]
                    openCamera()
                } else {
                    askForCameraPermissions()
                }

            } else {
                dialog.dismiss()
            }
        }
        dialogBuilder.show()
    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncher.launch(cameraIntent)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val data: Intent? = result.data
            if (result.resultCode == Activity.RESULT_OK && userChosenOption == "Photo Library") {
                binding.imageView2.setImageURI(data?.data)
                binding.selectImageTextView.visibility = View.INVISIBLE
            } else if (result.resultCode == Activity.RESULT_OK && userChosenOption == "Camera") {
                binding.imageView2.setImageBitmap(data?.extras?.get("data") as Bitmap)
                binding.selectImageTextView.visibility = View.INVISIBLE
            }
        }

    private fun isGalleryPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun isCameraPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun askForGalleryPermissions(): Boolean {
        if (!isGalleryPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    private fun askForCameraPermissions(): Boolean {
        if (!isCameraPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this as Activity,
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_REQUEST_CODE
                )
            }
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    userChosenOption = "Photo Library"
                    openGalleryForImage()
                } else {
                    askForGalleryPermissions()
                }
                return
            }
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    userChosenOption = "Camera"
                    openCamera()
                } else {
                    askForCameraPermissions()
                }
                return
            }
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(this)
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { _, _ ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }
}