package com.example.ssbookstoreapplication.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.ssbookstoreapplication.databinding.ActivityMainBinding
import com.example.ssbookstoreapplication.view.BookListingActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.button.setOnClickListener {
            val userID = "SS"
            val password = "11111"
            val inputUserId = binding.editTextTextPersonName.text.toString()
            val inputPassword = binding.editTextTextPassword.text.toString()
            if (inputUserId != userID || inputPassword != password) {
                Toast.makeText(
                    this,
                    "Invalid username/password. \nPlease try again.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val intent = Intent(this, BookListingActivity::class.java)
                this.startActivity(intent)
            }
        }
    }
}