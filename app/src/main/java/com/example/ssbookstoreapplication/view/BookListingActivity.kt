package com.example.ssbookstoreapplication.view

import android.R
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ssbookstoreapplication.clickListener.BookClickListener
import com.example.ssbookstoreapplication.adapter.BookAdapter
import com.example.ssbookstoreapplication.databinding.ActivityBookListingBinding
import com.example.ssbookstoreapplication.helper.SwipeBookItemHelper
import com.example.ssbookstoreapplication.model.*
import com.example.ssbookstoreapplication.viewModel.BookViewModel

class BookListingActivity : AppCompatActivity(), BookClickListener {
    private lateinit var binding: ActivityBookListingBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var bookAdapter: BookAdapter
    private lateinit var viewModel: BookViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookListingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = false
        }
        binding.addButton.setOnClickListener {
            val intent = Intent(applicationContext, BookDetailsActivity::class.java)
            startActivity(intent)
        }

        binding.logoutButton.setOnClickListener {
            finish()
        }

        val bookListing = this
        recyclerView = binding.recyclerView
        viewModel = ViewModelProvider(this).get(BookViewModel::class.java)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = BookAdapter(booklist, bookListing)
        }

        val itemTouchHelper = ItemTouchHelper(object : SwipeBookItemHelper(binding.recyclerView) {
            override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
                val button: List<UnderlayButton>
                val deleteBtn = deleteButton()
                button = listOf(deleteBtn)
                return button
            }
        })
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()
        viewModel.addBook(booklist as ArrayList<Book>)
        bookAdapter = recyclerView.adapter as BookAdapter
        updateData()
        bookAdapter.notifyDataSetChanged()
    }

    override fun onClick(book: Book) {
        val intent = Intent(applicationContext, BookDetailsActivity::class.java)
        intent.putExtra(BOOK_EXTRA, book.id)
        startActivity(intent)
    }

    private fun deleteButton(): SwipeBookItemHelper.UnderlayButton {
        return SwipeBookItemHelper.UnderlayButton(
            this,
            "Delete",
            14.0f,
            R.color.holo_red_light,
            object : SwipeBookItemHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    val book = booklist[selectedPosition]
                    val builder = AlertDialog.Builder(this@BookListingActivity)
                    builder.setTitle("Alert")
                    builder.setMessage("Confirm to delete?")
                    builder.setPositiveButton("Yes", DialogInterface.OnClickListener { _, _ ->
                        viewModel.remove(book)
                        updateData()
                    })
                    builder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
                    builder.show()
                }
            })
    }

    private fun updateData() {
        viewModel.liveData.observe(this, Observer {
            recyclerView.adapter = BookAdapter(it, this)
        })
    }
}