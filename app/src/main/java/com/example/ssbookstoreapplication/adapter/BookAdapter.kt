package com.example.ssbookstoreapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ssbookstoreapplication.clickListener.BookClickListener
import com.example.ssbookstoreapplication.viewHolder.BookViewHolder
import com.example.ssbookstoreapplication.databinding.BookItemBinding
import com.example.ssbookstoreapplication.model.*
import com.example.ssbookstoreapplication.viewModel.BookViewModel

class BookAdapter(
    private var booklist: MutableList<Book>,
    private val clickListener: BookClickListener
) : RecyclerView.Adapter<BookViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val from = LayoutInflater.from(parent.context)
        val binding = BookItemBinding.inflate(from, parent, false)
        return BookViewHolder(binding, clickListener)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.createBook(booklist[position])
    }

    override fun getItemCount(): Int {
        return booklist.size
    }

    fun removeBookItem(position: Int) {
        booklist.removeAt(position)
        notifyItemRemoved(position)
    }
}
