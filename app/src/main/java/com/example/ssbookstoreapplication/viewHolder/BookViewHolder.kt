package com.example.ssbookstoreapplication.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.example.ssbookstoreapplication.clickListener.BookClickListener
import com.example.ssbookstoreapplication.databinding.BookItemBinding
import com.example.ssbookstoreapplication.model.*

class BookViewHolder(
    private val bookItemBinding: BookItemBinding,
    private val clickListener: BookClickListener
) : RecyclerView.ViewHolder(bookItemBinding.root) {

    fun createBook(book: Book) {
        bookItemBinding.imageView.setImageBitmap(book.bookImage)
        bookItemBinding.bookNameTextView.text = book.bookName
        bookItemBinding.authorNameTextView.text = book.authorName
        bookItemBinding.dateTextView.text = book.dateCreated

        bookItemBinding.cardView.setOnClickListener {
            clickListener.onClick(book)
        }
    }
}