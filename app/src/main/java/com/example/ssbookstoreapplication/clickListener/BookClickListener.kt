package com.example.ssbookstoreapplication.clickListener

import com.example.ssbookstoreapplication.model.*

interface BookClickListener {
    fun onClick(book: Book)
}