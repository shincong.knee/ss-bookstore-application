package com.example.ssbookstoreapplication.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ssbookstoreapplication.model.Book

class BookViewModel: ViewModel() {
    var liveData = MutableLiveData<ArrayList<Book>>()

    fun addBook(bookList: ArrayList<Book>) {
        liveData.postValue(bookList)
    }

    fun remove(book: Book) {
        liveData.value?.remove(book)
    }
}