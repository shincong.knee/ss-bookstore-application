package com.example.ssbookstoreapplication.model

import android.graphics.Bitmap

var booklist = mutableListOf<Book>()
var isEdit: Boolean = false
const val BOOK_EXTRA = "bookExtra"
const val GALLERY_REQUEST_CODE = 100
const val CAMERA_REQUEST_CODE = 101
var selectedPosition: Int = 0

class Book(
    var bookImage: Bitmap,
    var bookName: String,
    var authorName: String,
    var description: String,
    var dateCreated: String,
    val id: Int? = booklist.size
)